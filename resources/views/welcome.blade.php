<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link rel="stylesheet" type="text/css" href="/css/semantic/dist/semantic.min.css">
        <style>
              body {
              background-color: #ffffff;
            }
            body > .grid {
              height: 100%;
            }
            /* .image {
              margin-top: -100px;
            } */
            .full-height {
            height: 100vh;
            }
            .back{
                background-size: cover;
                position: absolute;
                background-position: center;
                background-repeat: no-repeat;
                height: 100%;
                width:100%;
                background-image: url('/back.png');
            }
            label,input,a,button,h1,p{
                font-family: MiriamLibre-Regular !important;
            }
            .head{
                font-family: MiriamLibre-Regular !important;
                font-weight: bold !important;
            }
            .ui.form .field>label{
                text-align: left;
            }
            .ui.middle.aligned.center.aligned.grid .column .ui.form .field p{
                color: #8A42C1;
                font-size: 18px;
                margin-bottom: 2em;
            }
            .flex-center {
              align-items: center;
              display: flex;
              justify-content: center;
            }

            .position-ref {
              position: relative;
            }
        </style>
    </head>
    <body>
    <div class="default-layout flex-center position-ref full-height">
        <div class="ui middle aligned center aligned container grid" id="login">
            <div class="column" style="margin:15px;max-width: 450px;">
                <form class="ui form login">
                    <div class="field">
                        <div class="ui tiny images">
                           <h1 id="bigOne"></h1>
                        </div>
                    </div>
                    <div class="field">
                        <p>Simply notify</p>
                    </div>
                    <div class="field">
                        <label>Email Address</label>
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="email" placeholder="name@example.com">
                        </div>
                    </div>

                    <div class="field">
                        <label>Password</label>
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password"  name="password" id="user-password">
                        </div>
                    </div>
                    <button class="ui fluid submit button" id="sign-in">SIGN IN</button>
                </form>
            </div>
        </div>
      </div>
    </body>
    <script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
    <script src="/js/main.js"></script> 
</html>
